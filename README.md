# connect-four

Chok and Randy duke it out in this classic game of Connect Four.

Built with HTML, CSS, and JavaScript.

A Kenzie Academy SEQ1 group project by:
Koren @korenenyles
Mauricio @AlvaradoM
Joey @joeymizell
Sean @sepabailey
Ben @techieben
